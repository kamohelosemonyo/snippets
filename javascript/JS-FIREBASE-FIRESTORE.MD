# Firebase Cloud Firestore Javascript Snippets

## Firestore Api

Code snippets for crud operations for firebase

```js
const uuid = require('uuid')
const firebase = require('firebase')
require('firebase/firestore')

const config = {}
const app = firebase.default.initializeApp(config)
const firestore = app.firestore()


exports.list = async function (table, store = firestore) {
  const snapShot = await store.collection(table).get()
  return snapShot.docs.map((doc) => doc.data())
}

exports.get = async function (table, filter, store = firestore) {
  const tableRef = store.collection(table)
  const filterArray = keyValueToArray(filter)
  const snapShot = await tableRef.where(
    filterArray[0], '==', filterArray[1]
  ).get()

  return snapShot.docs.map((doc) => doc.data())[0]
}

exports.create = async function (table, object, store = firestore) {
  const docId = uuid.v1()
  object.id = docId
  const tableRef = store.collection(table)
  return await tableRef.doc(docId).set(object)
}

exports.update = async function (table, filter, object, strore = firestore) {
  const tableRef = strore.collection(table)
  const response = tableRef.doc(filter.id).update(object)
  return response
}

exports.delete = async function (table, filter, store = firestore) {
  if (filter.id === undefined) {
    return deleteWhere(table, filter, store)
  }

  const tableRef = store.collection(table)
  const response = await tableRef.doc(filter.id).delete()
  return response
}

async function deleteWhere (table, filter, store = firestore) {
  const tableRef = store.collection(table)
  const filterArray = keyValueToArray(filter)
  
  const snapShot = await tableRef.where(
    filterArray[0], '==', filterArray[1]
  ).get()

  const item = snapShot.docs.map((doc) => doc.data())[0]
  
  if (item == undefined) {
    return Promise.resolve('')
  }

  const response = await store.collection(table).doc(item.id).delete()
  return response
}

function keyValueToArray (filter) {
  const keys = Object.keys(filter)
  if (keys.length > 1) {
    throw Error(`too many filters provided ${filter}`)
  }

  return [keys[0], filter[keys[0]]]
}
```

## Unit tests

```js
const assert = require('assert')
const firesore = require('path-to-file')

describe ('Firestore api wrapper tests', function () {
  const table = 'test'

  it ('list function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      await firesore.list(table, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('list function called exactly once', async function () {
    const storeStub = getFirebaseStub()
    await firesore.list(table, storeStub)
    assert.deepStrictEqual(storeStub.getCount, 1)
  })

  it ('get function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      await firesore.get(table, { id: 'numnum'}, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('get function called exactly once', async function () {
    const storeStub = getFirebaseStub()
    await firesore.get(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.getCount, 1)
  })

  it ('get function where key set correctly', async function () {
    const storeStub = getFirebaseStub()
    await firesore.get(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.keyName, 'id')
  })

  it ('get function where operator set correctly', async function () {
    const storeStub = getFirebaseStub()
    await firesore.get(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.operatorName, '==')
  })

  it ('get function where value set correctly', async function () {
    const storeStub = getFirebaseStub()
    await firesore.get(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.valueName, 'numnum')
  })

  it ('create function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      await firesore.create(table, { id: 'numnum'}, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('create function object set correctly', async function () {
    const storeStub = getFirebaseStub()
    const obj = { name: 'jim', surname: 'jam'}
    await firesore.create(table, obj, storeStub)
    assert.deepStrictEqual(storeStub.objectRef, obj)
  })

  it ('update function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      const filter =  { id: 'numnum'}
      const obj = { name: 'jim', surname: 'jam'}
      await firesore.update(table, filter, obj, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('update function doc set correctly', async function () {
    const storeStub = getFirebaseStub()
    const filter =  { id: 'numnum'}
    const obj = { name: 'jim', surname: 'jam'}
    await firesore.update(table, filter, obj, storeStub)
    assert.deepStrictEqual(storeStub.docId, filter.id)
  })

  it ('update function object set correctly', async function () {
    const storeStub = getFirebaseStub()
    const filter =  { id: 'numnum'}
    const obj = { name: 'jim', surname: 'jam'}
    await firesore.update(table, filter, obj, storeStub)
    assert.deepStrictEqual(storeStub.objectRef, obj)
  })


  it ('delete function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      await firesore.delete(table, { id: 'numnum'}, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('delete function called exactly once', async function () {
    const storeStub = getFirebaseStub()
    await firesore.delete(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.deleteCount, 1)
  })

  it ('delete function doc id set correctly', async function () {
    const storeStub = getFirebaseStub()
    await firesore.delete(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.docId, 'numnum')
  })
})

function getFirebaseStub () {
  return {
    colname: '',
    docId: '',
    keyName: '',
    operatorName: '',
    valueName: '',
    objectRef: '',
    getCount: 0,
    deleteCount: 0,
    docs: [],
    collection: function (name) {
        this.colname = name
        return this
    },
    doc: function (name) {
        this.docId = name
        return this
    },
    set: function (object) {
      this.objectRef = object
      return this
    },
    update: function (object) {
      this.objectRef = object
      return this
    },
    get: function () { 
      this.getCount = this.getCount + 1
      return this
    },
    delete: function () { 
      this.deleteCount = this.deleteCount + 1
      return this
    },
    where: function (key, operator, value) {
      this.keyName = key
      this.operatorName = operator
      this.valueName = value
      return this
    },
  }
}

const col = getFirebaseStub().collection()

```