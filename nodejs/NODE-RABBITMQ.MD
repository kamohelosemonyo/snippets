# Rabbit MQ Node js Utitlity

# Module

```js
const amqp = require('amqplib/callback_api')
const logger = require('./logger')

const queue = { 
    host: host,
    user: user,
    password: password
}
let sender
let receiver

getConnection((connection) => { sender = connection })
getConnection((connection) => { receiver = connection })

function getConnection (callback) {
  const url = `amqp://${queue.user}:${queue.password}@${queue.host}`
  amqp.connect(url, function (err, connection) {
    if (err) {
      logger.error('Could not connect to RabbitMQ: ' + err)

      if (process.env.NODE_ENV === 'production') {
        process.exit(1)
      }

      callback(null)
    }
    callback(connection)
  }
  )
}

function waitBeforeAction (callback) {
  if (sender === undefined || receiver === undefined) {
    setTimeout(function () {
      callback()
    }, 3000)
  } else {
    callback()
  }
}

function getChannel (type, callback) {
  if (type === 'send') {
    waitBeforeAction(() => sender.createChannel((err, channel) => {
      if (err) {
        logger.error('could not create channel: ' + err)
      }
      callback(channel)
    })
    )
  } else {
    waitBeforeAction(() => receiver.createChannel((err, channel) => {
      if (err) {
        logger.error('could not create channel: ' + err)
      }
      callback(channel)
    })
    )
  }
}

exports.publish = function (exchange, message) {
  getChannel('send', function (channel) {
    channel.assertExchange(exchange, 'fanout', { durable: false })
    channel.publish(exchange, '', Buffer.from(message), { })
  })
}

exports.formatMessage = function (event, payload) {
  return JSON.stringify(
    { event: event, payload: payload }
  )
}

exports.subscribe = function (exchange, queuename, callback) {
  getChannel('receive', function (channel) {
    fanOutChannel(exchange, channel)
    listen(exchange, channel, queuename, function (message) {
      callback(message)
    })
  })
}

function fanOutChannel (exchange, channel) {
  channel.assertExchange(exchange, 'fanout', { durable: false })
}

function listen (exchange, channel, queuename, callback) {
  channel.assertQueue(queuename, { exclusive: false },
    function (err, queue) {
      if (err) {
        logger.error('could not open queue: ' + err)
      }
      channel.bindQueue(queue.queue, exchange, '')
      channel.consume(queue.queue, (message) => {
        channel.ack(message)
        callback(message.content.toString())
      })
    }
  )
}
```

## Unit tests

```js

```